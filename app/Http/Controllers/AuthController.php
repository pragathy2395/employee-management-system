<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api',['except'=>['login']]);
    }

    public function login($request){
        $credentials = $request->only('email','password');
        if($token    = $this->guard()->attempt($credentials)){
            return respondWithToken($token);
        }
        return response()->json(['error' => 'Invalid credentials'],'401');
    }

    public function logout(){
        $this->guard()->logout();
    }

    public function me(){
        dump($this->guard()->user());
        return $this->guard()->user();
    }

    public function respondWithToken($token){
        //$returnResponse = array();
       
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'status'       => 'success'
        ]);
    }

    public function guard(){
        dump(Auth::guard());
        return Auth::guard();
    }
}





















// public function refresh(){
    //     $this->guard()->refresh();
// }