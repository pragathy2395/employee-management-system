<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Employer;
use Employee;
use User;

class AdminController extends Controller
{
    public function index(){
        //dd("innn");exit();
        return view('admin');
    }

    public function store($request){
       dump($request->all());


    }

    public function edit($id){
        $user = User::find($id);
        if($user->role == 'Employer'){
            return view('employer',compact(['data' => $user]));
        }else if($user->role == 'Employee'){
            return view('employee',compact(['data' => $user]));
        }else{
            dd('User doesnot exists!..');
        }
    }

    public function delete($id){
        $user = User::find($id);
        if($user){
            $user->delete();
        }else{
            dd('User doesnot exists!..');
        }
    }
}
