<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use User;
use Employee;

class EmployeeController extends Controller
{
    public function index(){
        // dd("1233");exit;
        return view('addemployee');
    }

    public function addEmployee(Request $request){
        // dd("00888");exit;
        // dump($request);exit;
        $users     = User::create(['email' => $request->email,'password' => $request->password,'role'=>'Employee']);
        $employees = Employee::create(['name' => $request->name,'education' => $request->education,'experience'=>$request->experience, 'address'=>$request->address]);
        return view('admin');
    }

    public function viewEmployeeById($id){
        $user = User::find($id);
        if($user){
           return view('editemployee',compact(['data'=>$user]));
        }else{
            dd('Employee doesnot exists!..');
        }
    }

    public function editEmployee($request){
        $user = User::find($id);
        if($user->role == 'Employee'){
            $user->name  = $request->name;
            $user->email = $request->email;
            //$user->name  = $request->email;
            $user->save();
        }else{
            dd('Employee doesnot exists!..');
        }
    }

    public function viewEmployee(){
        $users = User::where('role','employee')->get()->all();
        if($users){
            return view('employee',compact(['data' => $users]));
        }else{
            dd("No employees found!..");
        }
        
    }

    public function deleteEmployee($id){
        $user = User::find($id);
        if($user){
            $user->delete();
        }else{
            dd('Employee doesnot exists!..');
        }
    }
}
