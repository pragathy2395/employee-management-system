<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table    = 'employees';
    protected $fillable = ['id', 'name', 'highest_education', 'address', 'experince'];
    public function Employee(){
        //$this->hasMany('Employee','id');
    }
}
