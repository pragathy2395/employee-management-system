<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'middleware'=>'api',
    'namespace' =>'App\Http\Controllers',
    'prefix'    =>'auth'
],function(){ //$router
    Route::post('login','AuthController@login');
    Route::post('logout','AuthController@logout');
    // Route::post('refresh','AuthController@refresh');
    Route::post('me','AuthController@me');
});

Route::get('/','AdminController@index');
Route::post('save','AdminController@store');
// Route::post('useredit/{id}','AdminController@edit');
// Route::post('userdel/{id}','AdminController@delete');

//employee
Route::get('index','EmployeeController@index');
Route::post('addEmployee','EmployeeController@addEmployee');
Route::post('editEmployee/{id}','EmployeeController@editEmployee');
Route::post('viewEmployeeById/{id}','EmployeeController@viewEmployeeById');
Route::post('viewEmployee','EmployeeController@viewEmployee');
Route::post('deleteEmployee/{id}','EmployeeController@deleteEmployee');

//employer
Route::post('addEmployer/{id}','EmployerController@addEmployer');
Route::post('editEmployer/{id}','EmployerController@editEmployer');
Route::post('viewEmployer','EmployerController@viewEmployer');
Route::post('deleteEmployer/{id}','EmployerController@deleteEmployer');

// Route::get('/', function () {
//     return view('welcome');
// });
